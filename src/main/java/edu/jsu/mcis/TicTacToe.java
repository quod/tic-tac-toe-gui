package tic.tac.toe.gui;

import java.awt.*;
import javax.swing.*;

public class TicTacToeGUI extends javax.swing.JFrame {
    
    String player;
        
    public TicTacToeGUI() {
        initComponents();
    }

    public void switchPlayer() {
        if(player.equals("X")) player = "O";
        else if(player.equals("O")) player = "X";
    }
    
    @SuppressWarnings("unchecked")

    public void initComponents() {
        JTextField box1 = new JTextField();
        JTextField box2 = new JTextField();
        JTextField box3 = new JTextField();
        JTextField box4 = new JTextField();
        JTextField box5 = new JTextField();
        JTextField box6 = new JTextField();
        JTextField box7 = new JTextField();
        JTextField box8 = new JTextField();
        JTextField box9 = new JTextField();
        
        JButton newGame;
        newGame = new JButton();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        box1.setEditable(false);
        box1.setFocusable(false);
        box1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box1.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box1.setText(player);
                switchPlayer();
            }
        });
        
        box2.setEditable(false);
        box2.setFocusable(false);
        box2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box2.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box2.setText(player);
                switchPlayer();
            }
        });

        box3.setEditable(false);
        box3.setFocusable(false);
        box3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box3.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box3.setText(player);
                switchPlayer();
            }
        });

        box4.setEditable(false);
        box4.setFocusable(false);
        box4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box4.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box4.setText(player);
                switchPlayer();
            }
        });

        box5.setEditable(false);
        box5.setFocusable(false);
        box5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box5.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box5.setText(player);
                switchPlayer();
            }
        });

        box6.setEditable(false);
        box6.setFocusable(false);
        box6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box6.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box6.setText(player);
                switchPlayer();
            }
        });

        box7.setEditable(false);
        box7.setFocusable(false);
        box7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box7.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box7.setText(player);
                switchPlayer();
            }
        });

        box8.setEditable(false);
        box8.setFocusable(false);
        box8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box8.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void onClick(java.awt.event.MouseEvent e) {
                box8.setText(player);
                switchPlayer();
            }
        });

        box9.setEditable(false);
        box9.setFocusable(false);
        box9.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        box9.setFont(new java.awt.Font("Droid Sans", 0, 100));
        box9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void OnClick(java.awt.event.MouseEvent e) {
                box9.setText(player);
                switchPlayer();
            }
        });

        newGame.setBackground(new java.awt.Color(222, 222, 222));
        newGame.setFont(newGame.getFont().deriveFont(newGame.getFont().getSize()+6f));
        newGame.setForeground(new java.awt.Color(0, 0, 0));
        newGame.setText("New Game");
        newGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newGame();
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>    
/*public void newGame(java.awt.event.ActionEvent e) {
    box1.setText("");
    box2.setText("");
    box3.setText("");
    box4.setText("");
    box5.setText("");
    box6.setText("");
    box7.setText("");
    box8.setText("");
    box9.setText("");
}*/


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TicTacToeGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TicTacToeGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TicTacToeGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TicTacToeGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TicTacToeGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
